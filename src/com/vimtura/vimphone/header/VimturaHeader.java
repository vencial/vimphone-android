package com.vimtura.vimphone.header;

import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCallParams;

public class VimturaHeader {
	
	protected LinphoneCall call;	
	protected VimturaContact contact;
	protected VimturaQueue queue;
	
	public VimturaHeader(LinphoneCall call) {
		this.call = call;
		this.contact = new VimturaContact(this);
		
		if (this.hasHeader(VimturaQueue.HEADER_QUEUE_NAME)) {
			this.queue = new VimturaQueue(this);
		}
	}

	public LinphoneCall getCall() {
		return call;
	}

	public VimturaContact getContact() {
		return contact;
	}

	public VimturaQueue getQueue() {
		return queue;
	}

	public String getHeader(String key) {
		if (this.getCall() != null) {
			LinphoneCallParams callParams = this.getCall().getRemoteParams();
			if (callParams != null) {
				return callParams.getCustomHeader(key);
			}
		}	
		return null;
	}	
	
	public boolean hasHeader(String key) {
		return (this.getHeader(key) != null);
	}
	
	public String toString() {
		return (new StringBuilder())
			.append("{ VimturaHeader [")
			.append(" contact: ").append(contact)
			.append(", queue: ").append(queue)			
			.append("] }")
			.toString();
	}
	
}
