package com.vimtura.vimphone.status;

import android.graphics.Color;

import com.vimtura.vimphone.R;

public enum StatusType {
	ORANGE(R.drawable.status_orange, null, null, null, null),
	GREEN(R.drawable.status_green, null, Color.CYAN, 300, 100),
	RED(R.drawable.status_red, null, Color.RED, 300, 100),
	OFFLINE(R.drawable.status_offline, null, null, null, null);
	
	private Integer smallIcon;
	private Integer largeIcon;
	private Integer lightRgb;
	private Integer lightOnMs;
	private Integer lightOffMs;
	
	StatusType(Integer smallIcon, Integer largeIcon, Integer lightRgb, Integer lightOnMs, Integer lightOffMs) {
		this.smallIcon = smallIcon;
		this.largeIcon = largeIcon;
		this.lightRgb = lightRgb;
		this.lightOnMs = lightOnMs;
		this.lightOffMs = lightOffMs;
	}

	public Integer getSmallIcon() {
		return smallIcon;
	}

	public Integer getLargeIcon() {
		return largeIcon;
	}

	public Integer getLightRgb() {
		return lightRgb;
	}

	public Integer getLightOnMs() {
		return lightOnMs;
	}

	public Integer getLightOffMs() {
		return lightOffMs;
	}
	
}
